class Author < ApplicationRecord
	has_many :books

	#Se tiver menos que 3 letras não vai adicionar na database
	validates :first_name, presence:true
	validates :last_name, presence:true
end
