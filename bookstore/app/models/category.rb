class Category < ApplicationRecord
	has_many :books

	#Se tiver menos que 3 letras não vai adicionar na database
	validates :name, presence:true,
				length: {minimum:3}
end
